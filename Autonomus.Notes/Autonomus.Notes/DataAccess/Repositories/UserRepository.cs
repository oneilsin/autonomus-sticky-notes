﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.EFramework;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected readonly NoteContext _efContext;
        public UserRepository(NoteContext noteContext)
        {
            _efContext = noteContext;
        }

        public async Task<IEnumerable<Usuary>> GetLogin(string usuario)
        {
            var response = await _efContext.Set<Usuary>().ToListAsync();
            return response.Where(o=>o.UserName.Equals(usuario));
        }
    }
}
