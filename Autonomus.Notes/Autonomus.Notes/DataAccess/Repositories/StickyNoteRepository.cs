﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.EFramework;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.DataAccess.SQLite.Queries;
using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Queries.StickyQueries;
using Autonomus.Notes.Domain.Repositories;
using Autonomus.Notes.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess.Repositories
{
    public class StickyNoteRepository : IStickyNoteRepository
    {
        protected readonly NoteContext _efContext;

        public StickyNoteRepository(NoteContext noteContext)
        {
            _efContext = noteContext;
        }



        public async Task<IEnumerable<StickyQuery>> GetListado(StickyQueryParams @params)
        {
            var response =  await _efContext.Set<StickyNote>().ToListAsync();

            var list = new List<StickyNote>();
            if (@params.FilterDate)
                list = response.ToList().Where(o => o.Completed.Equals(@params.Completed)
                && o.IdUser.Equals(@params.IdUser)
                && o.CreatedDate >= @params.StartDate && o.CreatedDate <= @params.EndDate).ToList();
            else
                list = response.ToList().Where(o => o.Completed.Equals(@params.Completed)
               && o.IdUser.Equals(@params.IdUser)).ToList();

            return list.Select((obj) => new StickyQuery()
            {
                Id = obj.Id,
                Cliente = obj.Cliente,
                CreatedDate = obj.CreatedDate,
                EditedDate = obj.EditedDate,
                Content = obj.Content,
                Completed = obj.Completed,
                Priority = obj.Priority,
                Height = obj.Height,
            });
        }

        public async Task<string> GetMaxId()
        {
            return await _efContext.Set<StickyNote>().MaxAsync(o => o.Id);
        }

        public async Task<int> SetData(StickyNote note, string accion)
        {
            //Insert, Update, Delete --- action
            if (accion.ToLower() == "insert")
            {
                var max = CusData.GetNoteCorrelative(await GetMaxId());
                note.Id = max;

                _efContext.Set<StickyNote>().Add(note);
            }
            else if (accion.ToLower() == "update")
            {
                var data = await _efContext.Set<StickyNote>().FirstOrDefaultAsync(o=>o.Id.Equals(note.Id));
                data.EditedDate = note.EditedDate;
                data.Cliente = note.Cliente;
                data.Content = note.Content;
                data.Completed = note.Completed;
                data.Height = note.Height;

                _efContext.Entry<StickyNote>(data).State= System.Data.Entity.EntityState.Modified;
            }
            else if (accion.ToLower() == "delete")
            {
                //
            }

            return await _efContext.SaveChangesAsync();
        }
    }
}
