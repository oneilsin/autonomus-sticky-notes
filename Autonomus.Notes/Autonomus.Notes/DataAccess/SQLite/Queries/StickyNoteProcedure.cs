﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.DataAccess.SQLite.Queries
{
    public class StickyNoteProcedure
    {
        public string GetListWithoutDateQueries(string idUser, bool completed)
        {
			int _cmpt = completed ? 1 : 0;

			return $@"SELECT Id,
					Cliente,
					CreatedDate,
					EditedDate,
					Content,
					Completed,
					Priority,
					Height
				FROM StickyNote
				WHERE Deleted=0
				AND IdUser='{idUser}'
				AND Completed={_cmpt}";

		}

		public string GetListWithDateRageQueries(string idUser, bool completed,
			DateTime startDate, DateTime endDate)
        {
			int _cmpt = completed ? 1 : 0;
			string _start = startDate.ToShortDateString();
			string _end = endDate.ToShortDateString();

			return $@"SELECT Id,
					Cliente,
					CreatedDate,
					EditedDate,
					Content,
					Completed,
					Priority,
					Height
				FROM StickyNote
				WHERE Deleted=0
				AND IdUser='{idUser}'
				AND Completed='{_cmpt}'
				AND SUBSTR(CreatedDate,1,10)>='{_start}'
				AND SUBSTR(CreatedDate,1,10)<='{_end}'";
        }
		public string GetListWithDateRageMSQueries(string idUser, bool completed,
			DateTime startDate, DateTime endDate)
		{
			int _cmpt = completed ? 1 : 0;
			return $@"SELECT Id,
					Cliente,
					CreatedDate,
					EditedDate,
					Content,
					Completed,
					Priority,
					Height
				FROM StickyNote
				WHERE Deleted=0
				AND IdUser='{idUser}'
				AND Completed={_cmpt}
				AND Format(CreatedDate,'Short Date')>='{startDate}'
                AND Format(CreatedDate,'Short Date')<='{endDate}'";
		}
		public string GetUpdateQueries(string cliente, string content, bool completed,
			int height, bool priority, string id)
        {
			int _cmpt = completed ? 1 : 0;
			int _prio = priority ? 1 : 0;

			return $@"UPDATE StickyNote
			SET EditedDate='{DateTime.Now}',
				Cliente='{cliente}',
				Content='{content}',
				Completed='{_cmpt}',
				Height='{height}',
				Priority='{_prio}'
			WHERE Id='{id}'";
        }


		public string GetMaxValueQueries()
        {
			return "SELECT ifnull(MAX(Id),0) AS Id FROM StickyNote";
		}


		public string GetMaxValueMSQueries()
        {
			return "SELECT IIf(MAX(Id) Is Null,0,MAX(Id))   FROM StickyNote";
		}

		public string GetInsertQueries(string id, string idUser)
        {

			return $@"INSERT INTO StickyNote
			VALUES('{id}',
				'',
				'{DateTime.Now}',
				'{DateTime.Now}',
				'',
				0,
				'{idUser}',
				0,
				0, 
				142
			)";
        }
    }
}
