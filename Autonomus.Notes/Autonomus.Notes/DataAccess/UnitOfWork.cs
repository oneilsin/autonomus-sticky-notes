﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.EFramework;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.Repositories;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.Domain;
using Autonomus.Notes.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private NoteContext _efContext;
        Servers servers = Servers.MSAccess;         
        public UnitOfWork()
        {
            string url = UrlConnection.GetUrlString(servers);
            this._efContext = new NoteContext(url);

            Stickys = new StickyNoteRepository(_efContext);
            Users = new UserRepository(_efContext);
        }

       
        public IStickyNoteRepository Stickys { get; private set; }
        public IUserRepository Users { get; private set; }

        public void Dispose()
        {
            _efContext.Dispose();
        }
    }
}
