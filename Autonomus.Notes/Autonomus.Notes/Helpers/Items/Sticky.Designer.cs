﻿namespace Autonomus.Notes.Helpers.Items
{
    partial class Sticky
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sticky));
            this.divContainer = new System.Windows.Forms.Panel();
            this._divider = new System.Windows.Forms.Label();
            this.divText = new System.Windows.Forms.Panel();
            this._text = new System.Windows.Forms.RichTextBox();
            this.divDate = new System.Windows.Forms.Panel();
            this._dateTime = new System.Windows.Forms.Label();
            this._editedDate = new System.Windows.Forms.Label();
            this._cliente = new System.Windows.Forms.TextBox();
            this.divControls = new System.Windows.Forms.Panel();
            this.btnUp = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnDown = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnList = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnDelete = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnPriority = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this._save = new System.Windows.Forms.Label();
            this._state = new System.Windows.Forms.CheckBox();
            this.btnUpdate = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.divContainer.SuspendLayout();
            this.divText.SuspendLayout();
            this.divDate.SuspendLayout();
            this.divControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // divContainer
            // 
            this.divContainer.BackColor = System.Drawing.Color.White;
            this.divContainer.Controls.Add(this._divider);
            this.divContainer.Controls.Add(this.divText);
            this.divContainer.Controls.Add(this.divControls);
            this.divContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divContainer.Location = new System.Drawing.Point(2, 2);
            this.divContainer.Name = "divContainer";
            this.divContainer.Size = new System.Drawing.Size(356, 138);
            this.divContainer.TabIndex = 1;
            // 
            // _divider
            // 
            this._divider.BackColor = System.Drawing.Color.WhiteSmoke;
            this._divider.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._divider.Font = new System.Drawing.Font("Verdana", 8F);
            this._divider.ForeColor = System.Drawing.Color.Gray;
            this._divider.Location = new System.Drawing.Point(0, 103);
            this._divider.Name = "_divider";
            this._divider.Size = new System.Drawing.Size(356, 1);
            this._divider.TabIndex = 5;
            // 
            // divText
            // 
            this.divText.Controls.Add(this._text);
            this.divText.Controls.Add(this.divDate);
            this.divText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divText.Location = new System.Drawing.Point(0, 0);
            this.divText.Name = "divText";
            this.divText.Padding = new System.Windows.Forms.Padding(3);
            this.divText.Size = new System.Drawing.Size(356, 104);
            this.divText.TabIndex = 0;
            // 
            // _text
            // 
            this._text.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._text.Dock = System.Windows.Forms.DockStyle.Fill;
            this._text.Font = new System.Drawing.Font("Comic Sans MS", 10.25F);
            this._text.Location = new System.Drawing.Point(3, 23);
            this._text.Name = "_text";
            this._text.Size = new System.Drawing.Size(350, 78);
            this._text.TabIndex = 0;
            this._text.Text = "";
            this._text.TextChanged += new System.EventHandler(this._text_TextChanged);
            // 
            // divDate
            // 
            this.divDate.Controls.Add(this._dateTime);
            this.divDate.Controls.Add(this._editedDate);
            this.divDate.Controls.Add(this._cliente);
            this.divDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.divDate.Location = new System.Drawing.Point(3, 3);
            this.divDate.Name = "divDate";
            this.divDate.Size = new System.Drawing.Size(350, 20);
            this.divDate.TabIndex = 0;
            // 
            // _dateTime
            // 
            this._dateTime.BackColor = System.Drawing.Color.White;
            this._dateTime.Dock = System.Windows.Forms.DockStyle.Right;
            this._dateTime.Font = new System.Drawing.Font("Comic Sans MS", 8.25F);
            this._dateTime.ForeColor = System.Drawing.Color.Gray;
            this._dateTime.Location = new System.Drawing.Point(110, 0);
            this._dateTime.Name = "_dateTime";
            this._dateTime.Size = new System.Drawing.Size(122, 20);
            this._dateTime.TabIndex = 0;
            this._dateTime.Text = "1901/01/01 00:00:00";
            this._dateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._dateTime.Visible = false;
            // 
            // _editedDate
            // 
            this._editedDate.AutoSize = true;
            this._editedDate.BackColor = System.Drawing.Color.White;
            this._editedDate.Dock = System.Windows.Forms.DockStyle.Right;
            this._editedDate.Font = new System.Drawing.Font("Comic Sans MS", 8.25F);
            this._editedDate.ForeColor = System.Drawing.Color.Gray;
            this._editedDate.Location = new System.Drawing.Point(232, 0);
            this._editedDate.Name = "_editedDate";
            this._editedDate.Size = new System.Drawing.Size(118, 15);
            this._editedDate.TabIndex = 1;
            this._editedDate.Text = "1901/01/01 00:00:00";
            this._editedDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _cliente
            // 
            this._cliente.BackColor = System.Drawing.Color.White;
            this._cliente.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._cliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this._cliente.Cursor = System.Windows.Forms.Cursors.Default;
            this._cliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cliente.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold);
            this._cliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(123)))), ((int)(((byte)(213)))));
            this._cliente.Location = new System.Drawing.Point(0, 0);
            this._cliente.MaxLength = 60;
            this._cliente.Name = "_cliente";
            this._cliente.Size = new System.Drawing.Size(350, 17);
            this._cliente.TabIndex = 0;
            this._cliente.TabStop = false;
            this._cliente.Text = "CLIENTE";
            this._cliente.Leave += new System.EventHandler(this._cliente_Leave);
            // 
            // divControls
            // 
            this.divControls.BackColor = System.Drawing.Color.White;
            this.divControls.Controls.Add(this.btnUp);
            this.divControls.Controls.Add(this.btnDown);
            this.divControls.Controls.Add(this.btnList);
            this.divControls.Controls.Add(this.btnDelete);
            this.divControls.Controls.Add(this.btnPriority);
            this.divControls.Controls.Add(this._save);
            this.divControls.Controls.Add(this._state);
            this.divControls.Controls.Add(this.btnUpdate);
            this.divControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.divControls.Location = new System.Drawing.Point(0, 104);
            this.divControls.Name = "divControls";
            this.divControls.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.divControls.Size = new System.Drawing.Size(356, 34);
            this.divControls.TabIndex = 1;
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUp.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnUp.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnUp.BorderRadius = 6;
            this.btnUp.BorderSize = 1;
            this.btnUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUp.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnUp.FlatAppearance.BorderSize = 0;
            this.btnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
            this.btnUp.Location = new System.Drawing.Point(162, 1);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(32, 32);
            this.btnUp.TabIndex = 9;
            this.btnUp.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUp.UseVisualStyleBackColor = false;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDown.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnDown.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnDown.BorderRadius = 6;
            this.btnDown.BorderSize = 1;
            this.btnDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDown.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDown.FlatAppearance.BorderSize = 0;
            this.btnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
            this.btnDown.Location = new System.Drawing.Point(194, 1);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(32, 32);
            this.btnDown.TabIndex = 8;
            this.btnDown.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDown.UseVisualStyleBackColor = false;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnList
            // 
            this.btnList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnList.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnList.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnList.BorderRadius = 6;
            this.btnList.BorderSize = 1;
            this.btnList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnList.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnList.FlatAppearance.BorderSize = 0;
            this.btnList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnList.Image = ((System.Drawing.Image)(resources.GetObject("btnList.Image")));
            this.btnList.Location = new System.Drawing.Point(226, 1);
            this.btnList.Name = "btnList";
            this.btnList.Size = new System.Drawing.Size(32, 32);
            this.btnList.TabIndex = 5;
            this.btnList.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnList.UseVisualStyleBackColor = false;
            this.btnList.Click += new System.EventHandler(this.btnList_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDelete.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnDelete.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnDelete.BorderRadius = 6;
            this.btnDelete.BorderSize = 1;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(258, 1);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnPriority
            // 
            this.btnPriority.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPriority.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnPriority.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnPriority.BorderRadius = 6;
            this.btnPriority.BorderSize = 1;
            this.btnPriority.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPriority.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPriority.FlatAppearance.BorderSize = 0;
            this.btnPriority.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPriority.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPriority.Image = global::Autonomus.Notes.Properties.Resources.star_empty_22px;
            this.btnPriority.Location = new System.Drawing.Point(290, 1);
            this.btnPriority.Name = "btnPriority";
            this.btnPriority.Size = new System.Drawing.Size(32, 32);
            this.btnPriority.TabIndex = 7;
            this.btnPriority.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPriority.UseVisualStyleBackColor = false;
            this.btnPriority.Click += new System.EventHandler(this.btnPriority_Click);
            // 
            // _save
            // 
            this._save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._save.AutoSize = true;
            this._save.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._save.ForeColor = System.Drawing.Color.Gray;
            this._save.Location = new System.Drawing.Point(126, 9);
            this._save.Name = "_save";
            this._save.Size = new System.Drawing.Size(24, 17);
            this._save.TabIndex = 6;
            this._save.Text = "SC";
            this._save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _state
            // 
            this._state.Cursor = System.Windows.Forms.Cursors.Hand;
            this._state.Dock = System.Windows.Forms.DockStyle.Left;
            this._state.Location = new System.Drawing.Point(2, 1);
            this._state.Name = "_state";
            this._state.Size = new System.Drawing.Size(104, 32);
            this._state.TabIndex = 0;
            this._state.Text = "Pendiente";
            this._state.UseVisualStyleBackColor = true;
            this._state.CheckedChanged += new System.EventHandler(this._state_CheckedChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUpdate.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnUpdate.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnUpdate.BorderRadius = 6;
            this.btnUpdate.BorderSize = 1;
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnUpdate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(322, 1);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(32, 32);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Sticky
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.divContainer);
            this.Name = "Sticky";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(360, 142);
            this.Load += new System.EventHandler(this.Sticky_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Sticky_KeyPress);
            this.divContainer.ResumeLayout(false);
            this.divText.ResumeLayout(false);
            this.divDate.ResumeLayout(false);
            this.divDate.PerformLayout();
            this.divControls.ResumeLayout(false);
            this.divControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel divContainer;
        private System.Windows.Forms.Label _divider;
        private System.Windows.Forms.Panel divText;
        private System.Windows.Forms.RichTextBox _text;
        private System.Windows.Forms.Panel divDate;
        private System.Windows.Forms.Label _dateTime;
        private System.Windows.Forms.Label _editedDate;
        private System.Windows.Forms.TextBox _cliente;
        private System.Windows.Forms.Panel divControls;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnUp;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnDown;
        private Autonomus.SDK.Components.CustomControls.AutonomusButton btnList;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnDelete;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnPriority;
        private System.Windows.Forms.Label _save;
        private System.Windows.Forms.CheckBox _state;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnUpdate;
    }
}
