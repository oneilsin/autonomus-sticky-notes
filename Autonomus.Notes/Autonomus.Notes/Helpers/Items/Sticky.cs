﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomus.Notes.Helpers.Items
{
    public partial class Sticky : UserControl
    {
        public DateTime Date { get { return Convert.ToDateTime(_dateTime.Text); } set { _dateTime.Text = value.ToString(); } }
        public DateTime EditedDate { get { return Convert.ToDateTime(_editedDate.Text); } set { _editedDate.Text = value.ToString(); } }
        public new string Text { get { return _text.Text; } set { _text.Text = value; } }
        public string Cliente { get { return _cliente.Text; } set { _cliente.Text = value; } }
        public string Id { get { return _id; } set { _id = value; } }
        public bool State { get { return _state.Checked; } set { _state.Checked = value; } }
        public int Index { get { return _index; } set { _index = value; } }
        public bool Priority
        {
            get { return _priority; }
            set
            {
                _priority = value;
                _favorite = (!value) ? Favorite.Yes : Favorite.Not;
                ChangePriority();
            }
        }

        #region before
        private string input;
        private bool _priority;
        private int _index;
        private string _id;
        private bool value;
        private Favorite _favorite;
        #endregion
        enum Favorite { Yes, Not }

        public Sticky()
        {
            InitializeComponent();
            _index = 0;
        }

        public void ChangePriority()
        {
            switch (_favorite)
            {
                case Favorite.Yes:
                    {
                        btnPriority.Image = Properties.Resources.star_empty_22px;
                        _favorite = Favorite.Not;
                        _priority = false;
                        break;
                    }
                case Favorite.Not:
                    {
                        btnPriority.Image = Properties.Resources.star_full_22px;
                        _favorite = Favorite.Yes;
                        _priority = true;
                        break;
                    }
            }
        }
        private void Changes()
        {
            input = _text.Text.Trim();
            value = _state.Checked;
            ValidateChange();
        }
        private void ValidateChange()
        {
            if (input != _text.Text.Trim())
            {
                _save.Text = "SC";
                return;
            }
            if (value != _state.Checked)
            {
                _save.Text = "SC";
                return;
            }
            _save.Text = string.Empty;
        }
        private void HeightUp()
        {
            this.Height = (this.Height - 16);
        }
        private void HeightDown()
        {
            this.Height = (this.Height + 16);
        }


        private void Sticky_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void _state_CheckedChanged(object sender, EventArgs e)
        {
            _state.Text = _state.Checked ? "Realizado" : "Pendiente";
            ValidateChange();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            HeightUp();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            HeightDown();
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            _text.Text = $"{_text.Text.Trim()}\n• ";
            _text.Focus();
            _text.SelectionStart = _text.Text.Length;
        }

        private void Sticky_Load(object sender, EventArgs e)
        {
            _text.Focus();
            Changes();
        }

        private void _text_TextChanged(object sender, EventArgs e)
        {
            ValidateChange();
        }

        private void btnPriority_Click(object sender, EventArgs e)
        {
            ChangePriority();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (_text.Text.Length <= 0) return;

            Changes();
            _save.Text = string.Empty;
        }

        private void _cliente_Leave(object sender, EventArgs e)
        {
            _text.Focus();
        }
    }
}
