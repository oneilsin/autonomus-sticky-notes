﻿using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Queries.StickyQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.Domain.Repositories
{
    public interface IStickyNoteRepository
    {
        Task<IEnumerable<StickyQuery>> GetListado(StickyQueryParams @params);
        Task<int> SetData(StickyNote note, string accion);
        Task<string> GetMaxId();
    }
}
