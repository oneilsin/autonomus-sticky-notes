USE [master]
GO
/****** Object:  Database [Notes]    Script Date: 04/02/2022 5:19:44 AM ******/
CREATE DATABASE [Notes]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Notes', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Notes.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Notes_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Notes_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Notes] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Notes].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Notes] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Notes] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Notes] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Notes] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Notes] SET ARITHABORT OFF 
GO
ALTER DATABASE [Notes] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Notes] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Notes] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Notes] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Notes] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Notes] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Notes] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Notes] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Notes] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Notes] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Notes] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Notes] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Notes] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Notes] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Notes] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Notes] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Notes] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Notes] SET RECOVERY FULL 
GO
ALTER DATABASE [Notes] SET  MULTI_USER 
GO
ALTER DATABASE [Notes] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Notes] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Notes] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Notes] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Notes] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Notes', N'ON'
GO
ALTER DATABASE [Notes] SET QUERY_STORE = OFF
GO
USE [Notes]
GO
/****** Object:  Table [dbo].[StickyNote]    Script Date: 04/02/2022 5:19:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StickyNote](
	[Id] [varchar](10) NOT NULL,
	[Cliente] [varchar](60) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[EditedDate] [datetime] NOT NULL,
	[Content] [varchar](max) NOT NULL,
	[Completed] [bit] NOT NULL,
	[IdUser] [varchar](4) NOT NULL,
	[Priority] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Height] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuary]    Script Date: 04/02/2022 5:19:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuary](
	[Id] [varchar](4) NOT NULL,
	[FullName] [varchar](100) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[UserName] [varchar](30) NOT NULL,
	[Password] [varchar](max) NOT NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020001', N'DORITA', CAST(N'2022-02-02T17:40:04.793' AS DateTime), CAST(N'2022-02-02T21:03:03.577' AS DateTime), N' comprar ppan  Ya esta', 1, N'0001', 0, 0, 110)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020002', N'TITULO', CAST(N'2022-02-02T19:20:00.270' AS DateTime), CAST(N'2022-02-02T19:22:42.407' AS DateTime), N'nota 1', 1, N'0001', 0, 0, 142)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020003', N'TITULO', CAST(N'2022-02-02T21:02:25.603' AS DateTime), CAST(N'2022-02-03T11:18:51.320' AS DateTime), N'ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico ir al mercado  y  comer algo rico :.dasdasdsd', 0, N'0001', 0, 0, 222)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020004', N'', CAST(N'2022-02-02T21:02:37.473' AS DateTime), CAST(N'2022-02-02T21:17:37.450' AS DateTime), N'hacer  video del las notas en c#', 1, N'0001', 1, 0, 94)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020005', N'', CAST(N'2022-02-02T21:14:24.383' AS DateTime), CAST(N'2022-02-02T21:14:24.383' AS DateTime), N'', 0, N'0001', 0, 1, 142)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020006', N'TELEFONO DE JUAN', CAST(N'2022-02-02T21:16:31.520' AS DateTime), CAST(N'2022-02-02T21:16:51.377' AS DateTime), N'recoger celular', 0, N'0001', 1, 1, 142)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020007', N'', CAST(N'2022-02-03T11:18:57.297' AS DateTime), CAST(N'2022-02-03T11:19:01.977' AS DateTime), N'asdasdsadasd', 0, N'0001', 0, 0, 142)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020008', N'', CAST(N'2022-02-03T11:40:45.907' AS DateTime), CAST(N'2022-02-03T11:40:45.907' AS DateTime), N'', 0, N'0001', 0, 0, 142)
INSERT [dbo].[StickyNote] ([Id], [Cliente], [CreatedDate], [EditedDate], [Content], [Completed], [IdUser], [Priority], [Deleted], [Height]) VALUES (N'2022020009', N'', CAST(N'2022-02-03T12:06:15.227' AS DateTime), CAST(N'2022-02-03T12:06:15.227' AS DateTime), N'', 0, N'0001', 0, 0, 142)
GO
INSERT [dbo].[Usuary] ([Id], [FullName], [Description], [UserName], [Password], [Deleted]) VALUES (N'0001', N'LEGUIA REYNAGA ALCIDES', N'OF|REDES E INFORMÁTICA', N'aleguia', N'Jockey123', 0)
GO
ALTER TABLE [dbo].[StickyNote]  WITH CHECK ADD FOREIGN KEY([IdUser])
REFERENCES [dbo].[Usuary] ([Id])
GO
/****** Object:  StoredProcedure [dbo].[UspStickyNoteMerge]    Script Date: 04/02/2022 5:19:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UspStickyNoteMerge]( 
	@Id VARCHAR(10),
	@Cliente VARCHAR(60),
	@Content VARCHAR(MAX),
	@Completed BIT,
	@IdUser VARCHAR(4),
	@Priority BIT,
	@Deleted BIT,
	@Height INT,
	--
	@action VARCHAR(10)
)AS BEGIN
	IF(@action='update') --UPDATE
		BEGIN
			UPDATE StickyNote
			SET EditedDate=GETDATE(),
				Cliente=@Cliente,
				Content=@Content,
				Completed=@Completed,
				Height=@Height
			WHERE Id=@Id
		END

	IF(@action='insert') --INSERT
		BEGIN
			DECLARE @year VARCHAR(4)=(YEAR(GETDATE()));
			DECLARE @month VARCHAR(2)=(FORMAT(MONTH(GETDATE()),'00'));

			DECLARE @newID VARCHAR(10)=(
				SELECT @year+@month+
					FORMAT(CONVERT(INT, RIGHT(ISNULL(MAX(Id),0)+1,4)),'0000')
				FROM StickyNote
				WHERE LEFT(Id,6)=(@year+@month)
			);

			INSERT INTO StickyNote
			VALUES(@newID,
				@Cliente,
				GETDATE(),
				GETDATE(),
				@Content,
				@Completed,
				@IdUser,
				@Priority,
				0, -- is not deleted
				@Height
			);
		END
	
	IF(@action='delete') --DELETE
		BEGIN
			UPDATE StickyNote
			SET Deleted=@Deleted
			WHERE Id=@Id
		END

	-- Change priority?
	IF(@action='priority') --PRIORITY
		BEGIN
			UPDATE StickyNote
			SET [Priority]=@Priority
			WHERE Id=@Id
		END

	IF(@action='height') --PRIORITY
		BEGIN
			UPDATE StickyNote
			SET Height=@Height
			WHERE Id=@Id
		END
		
END
GO
/****** Object:  StoredProcedure [dbo].[UspStickyNoteSearch]    Script Date: 04/02/2022 5:19:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UspStickyNoteSearch]( 
	@Completed BIT,
	@IdUser VARCHAR(4),
	@FilterDate BIT,
	@StartDate DATE,
	@EndDate DATE
)AS BEGIN
	IF(OBJECT_ID('tempdb..#tmpStiky') Is Not Null)
	BEGIN DROP TABLE #tmpStiky END
	SELECT Id,
		Cliente,
		CreatedDate,
		EditedDate,
		Content,
		Completed,
		[Priority],
		Height
	INTO #tmpStiky
	FROM StickyNote
	WHERE Deleted=0
	AND IdUser=@IdUser
	AND Completed=@Completed
	---------------------------------------------------------
	DECLARE @query NVARCHAR(MAX);
	IF(@FilterDate=1)
		BEGIN
			DECLARE @start VARCHAR(10) = @StartDate;--CONVERT(DATE,GETDATE())
			DECLARE @end VARCHAR(10) = @EndDate--;CONVERT(DATE,GETDATE())

			SET @query=('SELECT * FROM #tmpStiky
			WHERE CONVERT(DATE,CreatedDate) BETWEEN '''+@start+''' AND '''+@end+'''');
		END
	ELSE
		BEGIN
			SET @query=('SELECT * FROM #tmpStiky');
		END
	---------------------------------------------------------
	Exec sys.sp_executesql @query
END
GO
USE [master]
GO
ALTER DATABASE [Notes] SET  READ_WRITE 
GO
