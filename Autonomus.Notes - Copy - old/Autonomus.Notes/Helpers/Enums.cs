﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.Helpers
{
    public class Enums
    {
        public enum Crud { Insert, Update, Delete };
        public enum Login { Wrong, Success }
        public enum Servers { SQLServer, SQLite, MSAccess }
        public enum ORM { AdoNet, EF }

    }
}
