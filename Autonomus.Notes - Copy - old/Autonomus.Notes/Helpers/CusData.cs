﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.Helpers
{
    public class CusData
    {
        public static string GetNoteCorrelative(string maxValue)
        {
            if (maxValue.Length < 2)
            {
                return $"{year_month()}0001"; 
            }
            else
            {
                string ym = maxValue.Substring(0, 6);
                int corr = int.Parse(maxValue.Substring(6, 4));

                if (ym == year_month())
                    corr++;
                else
                    corr = 1;

                return $"{year_month()}{(string.Format("{0:0000}", corr))}";
            }          
        }

        static string year_month()
        {
            return string.Concat(DateTime.Now.Year, string.Format("{0:00}", DateTime.Now.Month));//20200100001
        }
    }
}
