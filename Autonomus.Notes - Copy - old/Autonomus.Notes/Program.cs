﻿using Autonomus.Notes.MVC.Views.LoginViews;
using Autonomus.Notes.MVC.Views.StickyViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomus.Notes
{
    internal static class Program
    {
        public static string IdUsuario = "0001";
        public static string Nombres = "Administrador";
        public static string Descripcion = "Custom - devloper";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //var login = new LoginView();
            //SDK.Forms.ShowDialog(login);

            //if (login.Login == Helpers.Enums.Login.Success)
                Application.Run(new StickyView());

        }
    }
}
