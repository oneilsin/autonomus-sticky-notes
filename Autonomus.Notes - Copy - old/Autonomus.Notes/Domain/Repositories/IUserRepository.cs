﻿using Autonomus.Notes.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<Usuary>> GetLogin(string usuario);
    }
}
