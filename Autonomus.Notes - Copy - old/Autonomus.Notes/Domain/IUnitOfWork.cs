﻿using Autonomus.Notes.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        IStickyNoteRepository Stickys { get; }
        IUserRepository Users { get; }
    }
}
