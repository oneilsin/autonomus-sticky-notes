﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AdoContext _adoContext;
        private readonly LiteContext _liteContext;
        private readonly OleContext _oleContext;
        Servers _servers;
        public UserRepository(AdoContext adoContext, Servers servers)
        {
            _adoContext = adoContext;
            _servers = servers;
        }

        public UserRepository(LiteContext liteContext, Servers servers)
        {
            _liteContext = liteContext;
            _servers = servers;
        }

        public UserRepository(OleContext oleContext, Servers servers)
        {
            _oleContext = oleContext;
            _servers = servers;
        }

        public async Task<IEnumerable<Usuary>> GetLogin(string usuario)
        {
            string query = $"SELECT * FROM Usuary WHERE UserName='{usuario}'";
            var response = await _adoContext.ExecuteReader<Usuary>(query, System.Data.CommandType.Text);

            return response;
        }
    }
}
