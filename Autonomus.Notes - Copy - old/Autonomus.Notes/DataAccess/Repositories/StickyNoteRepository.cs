﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.DataAccess.SQLite.Queries;
using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Queries.StickyQueries;
using Autonomus.Notes.Domain.Repositories;
using Autonomus.Notes.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess.Repositories
{
    public class StickyNoteRepository : IStickyNoteRepository
    {
        private readonly AdoContext _adoContext;
        private readonly LiteContext _liteContext;
        private readonly OleContext _oleContext;
        Servers _servers;
        private string litetUrl = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
        private string oleUrl;// = s();// ConfigurationManager.ConnectionStrings["MSAccess"].ConnectionString;

        string s()
        {
            var conexionString = ConfigurationManager.ConnectionStrings["MSAccess"].ConnectionString;
            var sb = new OleDbConnectionStringBuilder(conexionString);
            return sb.ToString();
        }

        public StickyNoteRepository(AdoContext adoContext, Servers servers)
        {
            _adoContext = adoContext;
            _servers = servers;
        }

        public StickyNoteRepository(LiteContext liteContext, Servers servers)
        {
            _liteContext = liteContext;
            _servers = servers;
        }

        public StickyNoteRepository(OleContext oleContext, Servers servers)
        {
            oleUrl = s();
            _oleContext = oleContext;
            _servers = servers;
        }

        public async Task<IEnumerable<StickyQuery>> GetListado(StickyQueryParams @params)
        {
            string sqlString = String.Empty;
            var response = new List<StickyQuery>();

            switch (_servers)
            {
                case Servers.SQLServer:
                    {
                        sqlString = "UspStickyNoteSearch";
                        var ret = await _adoContext.ExecuteReader<StickyQuery>(
                            sqlString,
                            System.Data.CommandType.StoredProcedure,
                            new System.Data.SqlClient.SqlParameter[]
                            {
                                new System.Data.SqlClient.SqlParameter("@Completed", @params.Completed),
                                new System.Data.SqlClient.SqlParameter("@IdUser", @params.IdUser),
                                new System.Data.SqlClient.SqlParameter("@FilterDate", @params.FilterDate),
                                new System.Data.SqlClient.SqlParameter("@StartDate", @params.StartDate),
                                new System.Data.SqlClient.SqlParameter("@EndDate", @params.EndDate)

                            });
                        response = ret.ToList();
                        break;
                    }
                case Servers.SQLite:
                    {
                        if (@params.FilterDate)
                            sqlString = new StickyNoteProcedure().GetListWithDateRageQueries(
                                @params.IdUser, @params.Completed, @params.StartDate, @params.EndDate);
                        else
                            sqlString = new StickyNoteProcedure().GetListWithoutDateQueries(
                                @params.IdUser, @params.Completed);

                        var ret = await _liteContext.ExecuteReader<StickyQuery>(sqlString);
                        response = ret.ToList();
                        break;
                    }
                case Servers.MSAccess:
                    {
                        if (@params.FilterDate)
                            sqlString = new StickyNoteProcedure().GetListWithDateRageMSQueries(
                                @params.IdUser, @params.Completed, @params.StartDate, @params.EndDate);
                        else
                            sqlString = new StickyNoteProcedure().GetListWithoutDateQueries(
                                @params.IdUser, @params.Completed);


                        //var ret = await _oleContext.ExecuteReader<StickyQuery>(sqlString);
                        //response = ret.ToList();

                          using (OleDbConnection ole = new OleDbConnection(oleUrl))
                          {
                              ole.Open();
                              using (OleDbCommand cm = new OleDbCommand(sqlString, ole))
                              {
                                  cm.CommandType = System.Data.CommandType.Text;
                                  var reader = cm.ExecuteReader();
                                  while (reader.Read())
                                  {
                                      bool com = int.Parse(reader["Completed"].ToString()) > 0 ? true : false;
                                      bool pri = int.Parse(reader["Priority"].ToString()) > 0 ? true : false;
                                      response.Add(new StickyQuery()
                                      {
                                          Id = reader["Id"].ToString(),
                                          Cliente = reader["Cliente"].ToString(),
                                          CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString()),
                                          EditedDate = Convert.ToDateTime(reader["EditedDate"].ToString()),
                                          Content = reader["Content"].ToString(),
                                          Completed = com,
                                          Priority = pri,
                                          Height = Convert.ToInt32(reader["Height"].ToString())
                                      });
                                  }
                              }
                          }

                        break;
                    }
            }


            return response;
        }

        public async Task<int> SetData(StickyNote note, string accion)
        {
            string sqlString = String.Empty;
            var response = 0;

            switch (_servers)
            {
                case Servers.SQLServer:
                    {
                        sqlString = "UspStickyNoteMerge";
                        var ret = await _adoContext.ExecuteNonQuery(
                           sqlString,
                           System.Data.CommandType.StoredProcedure,
                           new System.Data.SqlClient.SqlParameter[]
                           {
                                new System.Data.SqlClient.SqlParameter("@Id", note.Id),
                                new System.Data.SqlClient.SqlParameter("@Cliente", note.Cliente),
                                new System.Data.SqlClient.SqlParameter("@Content", note.Content),
                                new System.Data.SqlClient.SqlParameter("@Completed", note.Completed),
                                new System.Data.SqlClient.SqlParameter("@IdUser", note.IdUser),
                                new System.Data.SqlClient.SqlParameter("@Priority", note.Priority),
                                new System.Data.SqlClient.SqlParameter("@Deleted", note.Deleted),
                                new System.Data.SqlClient.SqlParameter("@Height", note.Height),
                                new System.Data.SqlClient.SqlParameter("@action", accion)

                           });
                        response = ret;
                        break;
                    }
                case Servers.SQLite:
                    {                       
                        //Insert, Update, Delete
                        if (accion.Equals("update"))
                            sqlString = new StickyNoteProcedure().GetUpdateQueries(
                                note.Cliente, note.Content, note.Completed,
                                note.Height, note.Priority, note.Id);

                        else if (accion.Equals("insert"))
                        {
                            var lite = new LiteContext(litetUrl);
                            var max = await lite.ExecuteEscalar(new StickyNoteProcedure().GetMaxValueQueries());

                            string newID = CusData.GetNoteCorrelative(max.ToString());

                            sqlString = new StickyNoteProcedure().GetInsertQueries(
                               newID, note.IdUser);
                        }
                        // else if (accion.Equals("delete"))
                        // query delete

                        var ret = await _liteContext.ExecuteNonQuery(sqlString);
                        response = ret;
                        break;
                    }
                case Servers.MSAccess:
                    {
                        //Insert, Update, Delete
                        if (accion.Equals("update"))
                            sqlString = new StickyNoteProcedure().GetUpdateQueries(
                                note.Cliente, note.Content, note.Completed,
                                note.Height, note.Priority, note.Id);

                        else if (accion.Equals("insert"))
                        {
                            var ole = new OleContext(oleUrl);
                            var max = await ole.ExecuteEscalar(new StickyNoteProcedure().GetMaxValueMSQueries());

                            string newID = CusData.GetNoteCorrelative(max.ToString());

                            sqlString = new StickyNoteProcedure().GetInsertQueries(
                               newID, note.IdUser);
                        }
                        // else if (accion.Equals("delete"))
                        // query delete

                        var ret = await _oleContext.ExecuteNonQuery(sqlString);
                        response = ret;
                        break;
                    }
            }



            return response;
        }
    }
}
