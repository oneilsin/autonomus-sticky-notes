﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.DataAccess.OleDbms
{
    public class OleContext
    {
        private OleDbConnection lite;
        public OleContext(string url)
        {
            lite = new OleDbConnection(url);
        }

        public void Dispose() { lite.Dispose(); }

        public async Task<Int32> ExecuteNonQuery(string querySql)
        {
            try
            {
                await lite.OpenAsync();
                using (OleDbCommand cm = new OleDbCommand(querySql, lite))
                {
                    cm.CommandType = CommandType.Text;

                    return await cm.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public async Task<Object> ExecuteEscalar(string querySql)
        {
            try
            {
                await lite.OpenAsync();
                using (OleDbCommand cm = new OleDbCommand(querySql, lite))
                {
                    cm.CommandType = CommandType.Text;
                    return await cm.ExecuteScalarAsync();
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public async Task<IEnumerable<T>> ExecuteReader<T>(string querySql) where T : class, new()
        {
            try
            {
                await lite.OpenAsync();
                using (OleDbCommand cm = new OleDbCommand(querySql, lite))
                {
                    cm.CommandType = CommandType.Text;

                    List<T> objectList = new List<T>();
                    using (var reader = await cm.ExecuteReaderAsync(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            T obj = new T();
                            foreach (var prop in obj.GetType().GetProperties())
                            {
                                try
                                {
                                    PropertyInfo info = obj.GetType().GetProperty(prop.Name);
                                    info.SetValue(obj, Convert.ChangeType(reader[prop.Name], info.PropertyType), null);
                                }
                                catch
                                { continue; }
                            }
                            objectList.Add(obj);
                        }
                    }
                    return objectList;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
