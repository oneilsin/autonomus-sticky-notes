﻿using Autonomus.Notes.DataAccess.AdoNet;
using Autonomus.Notes.DataAccess.EFramework;
using Autonomus.Notes.DataAccess.OleDbms;
using Autonomus.Notes.DataAccess.Repositories;
using Autonomus.Notes.DataAccess.SQLite;
using Autonomus.Notes.Domain;
using Autonomus.Notes.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        AdoContext _adoContext;
        LiteContext _liteContext;
        OleContext _oleContext;
        private NoteContext _efContext;

        Servers servers = Servers.MSAccess;
        ORM orm;
        public UnitOfWork()
        {
            string url = UrlConnection.GetUrlString(servers);
            this._efContext = new NoteContext(url);
            InitServer();
        }

        void InitServer()
        {
            string url = UrlConnection.GetUrlString(servers);

            switch (servers)
            {
                case Servers.SQLServer:
                    {
                        _adoContext = new AdoContext(url);

                        Stickys = new StickyNoteRepository(_adoContext, servers);
                        Users = new UserRepository(_adoContext, servers);
                        break;
                    }
                case Servers.SQLite:
                    {
                        _liteContext = new LiteContext(url);

                        Stickys = new StickyNoteRepository(_liteContext, servers);
                        Users = new UserRepository(_liteContext, servers);
                        break;
                    }
                case Servers.MSAccess:
                    {
                        _oleContext = new OleContext(url);

                        Stickys = new StickyNoteRepository(_oleContext, servers);
                        Users = new UserRepository(_oleContext, servers);
                        break;
                    }
            }
        }

        public IStickyNoteRepository Stickys { get; private set; }
        public IUserRepository Users { get; private set; }

        public void Dispose()
        {
            switch (servers)
            {
                case Servers.SQLServer:
                    {
                        _adoContext.Dispose();
                        break;
                    }
                case Servers.SQLite:
                    {
                        _liteContext.Dispose();
                        break;
                    }
                case Servers.MSAccess:
                    {
                        _oleContext.Dispose();
                        break;
                    }
            }

           
           
        }
    }
}
