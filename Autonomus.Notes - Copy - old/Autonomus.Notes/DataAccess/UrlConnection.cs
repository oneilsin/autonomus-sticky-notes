﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.DataAccess
{
    public class UrlConnection
    {
        public static string GetUrlString(Servers servers)
        {
            string name = servers == Servers.MSAccess ? "MSAccess": servers==Servers.SQLite ? "SQLite" : "SQLServer";
            return ConfigurationManager.ConnectionStrings[name].ConnectionString; 
        }
    }
}
