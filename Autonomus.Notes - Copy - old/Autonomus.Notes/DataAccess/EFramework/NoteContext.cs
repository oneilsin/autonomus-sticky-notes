﻿
using Autonomus.Notes.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.DataAccess.EFramework
{
    public class NoteContext : DbContext
    {
        public NoteContext(string url):base(url)
        {
            Database.SetInitializer<NoteContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Usuary> Usuaries { get; set; }
        public DbSet<StickyNote> Stickies { get; set; }
    }
}
