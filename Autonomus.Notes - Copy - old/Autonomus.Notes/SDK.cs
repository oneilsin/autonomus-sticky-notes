﻿using System;
using System.Windows.Forms;

namespace Autonomus.Notes
{
    internal class SDK
    {
        public class Forms
        {
            public static DialogResult ShowDialog(Form childForm)
            {
                return new Autonomus.SDK.Components.CustomClass.Forms().ShowDialog(childForm);
            }
        }
        public class Getting
        {
            public static DateTime GetFirtDayMonth()
            {
                return Autonomus.SDK.Components.CustomClass.GetCustomData.GetFirstDayMonth();
            }
        }
        public class MessageBoxs
        {
            public static DialogResult Show(string caption, string message, System.Windows.Forms.MessageBoxIcon type)
            {
                return
                new Autonomus.SDK.Components.CustomClass.MessageAlert()
                    .Show(message, caption, type);
            }
            public static DialogResult Error(string caption, string message)
            {
                return
                new Autonomus.SDK.Components.CustomClass.MessageAlert()
                    .Show(message, caption, System.Windows.Forms.MessageBoxIcon.Error);
            }
            public static DialogResult Question(string caption, string message)
            {
                return
                new Autonomus.SDK.Components.CustomClass.MessageAlert()
                    .Show(message, caption, System.Windows.Forms.MessageBoxIcon.Question);
            }
            public static DialogResult Complete(string caption, string message)
            {
                return
                new Autonomus.SDK.Components.CustomClass.MessageAlert()
                    .Show(message, caption, System.Windows.Forms.MessageBoxIcon.Information);
            }
            public static DialogResult Warning(string caption, string message)
            {
                return
                new Autonomus.SDK.Components.CustomClass.MessageAlert()
                    .Show(message, caption, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
}
