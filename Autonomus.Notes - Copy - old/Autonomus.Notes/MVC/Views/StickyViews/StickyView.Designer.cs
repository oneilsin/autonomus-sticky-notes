﻿namespace Autonomus.Notes.MVC.Views.StickyViews
{
    partial class StickyView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StickyView));
            this.divContent = new System.Windows.Forms.Panel();
            this._divider = new System.Windows.Forms.Label();
            this.divBody = new System.Windows.Forms.Panel();
            this.flowContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.divFooter = new System.Windows.Forms.Panel();
            this.endDate = new Autonomus.SDK.Components.CustomControls.AutonomusDateTimePicker();
            this.startDate = new Autonomus.SDK.Components.CustomControls.AutonomusDateTimePicker();
            this._bot = new System.Windows.Forms.Label();
            this.divHeader = new System.Windows.Forms.Panel();
            this._user = new System.Windows.Forms.Label();
            this._title = new System.Windows.Forms.Label();
            this._minimize = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this._exit = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.cbxDate = new System.Windows.Forms.CheckBox();
            this.cbxVer = new System.Windows.Forms.CheckBox();
            this._add = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnRefresh = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.divContent.SuspendLayout();
            this.divBody.SuspendLayout();
            this.divFooter.SuspendLayout();
            this.divHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // divContent
            // 
            this.divContent.Controls.Add(this._divider);
            this.divContent.Controls.Add(this.divBody);
            this.divContent.Controls.Add(this.divFooter);
            this.divContent.Controls.Add(this.divHeader);
            this.divContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divContent.Location = new System.Drawing.Point(8, 8);
            this.divContent.Name = "divContent";
            this.divContent.Size = new System.Drawing.Size(392, 555);
            this.divContent.TabIndex = 1;
            // 
            // _divider
            // 
            this._divider.BackColor = System.Drawing.Color.WhiteSmoke;
            this._divider.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._divider.Font = new System.Drawing.Font("Verdana", 8F);
            this._divider.ForeColor = System.Drawing.Color.Gray;
            this._divider.Location = new System.Drawing.Point(0, 522);
            this._divider.Name = "_divider";
            this._divider.Size = new System.Drawing.Size(392, 1);
            this._divider.TabIndex = 6;
            // 
            // divBody
            // 
            this.divBody.Controls.Add(this.flowContainer);
            this.divBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divBody.Location = new System.Drawing.Point(0, 32);
            this.divBody.Name = "divBody";
            this.divBody.Size = new System.Drawing.Size(392, 491);
            this.divBody.TabIndex = 0;
            // 
            // flowContainer
            // 
            this.flowContainer.AutoScroll = true;
            this.flowContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowContainer.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowContainer.Location = new System.Drawing.Point(0, 0);
            this.flowContainer.Name = "flowContainer";
            this.flowContainer.Size = new System.Drawing.Size(392, 491);
            this.flowContainer.TabIndex = 0;
            this.flowContainer.WrapContents = false;
            this.flowContainer.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.flowContainer_ControlAdded);
            this.flowContainer.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.flowContainer_ControlRemoved);
            // 
            // divFooter
            // 
            this.divFooter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.divFooter.Controls.Add(this._bot);
            this.divFooter.Controls.Add(this.cbxDate);
            this.divFooter.Controls.Add(this.cbxVer);
            this.divFooter.Controls.Add(this.startDate);
            this.divFooter.Controls.Add(this.endDate);
            this.divFooter.Controls.Add(this.btnRefresh);
            this.divFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.divFooter.Location = new System.Drawing.Point(0, 523);
            this.divFooter.Name = "divFooter";
            this.divFooter.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.divFooter.Size = new System.Drawing.Size(392, 32);
            this.divFooter.TabIndex = 0;
            // 
            // endDate
            // 
            this.endDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.endDate.Caption = "Caption text";
            this.endDate.CaptionFont = new System.Drawing.Font("Verdana", 9.25F);
            this.endDate.CaptionForeColor = System.Drawing.Color.DimGray;
            this.endDate.CaptionVisible = false;
            this.endDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.endDate.Enabled = false;
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDate.Info = "";
            this.endDate.InfoFont = new System.Drawing.Font("Verdana", 8.25F);
            this.endDate.InfoForeColor = System.Drawing.Color.Gray;
            this.endDate.InfoVisible = false;
            this.endDate.LineColor = System.Drawing.Color.Silver;
            this.endDate.Location = new System.Drawing.Point(293, 1);
            this.endDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.endDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.endDate.MinimumSize = new System.Drawing.Size(12, 12);
            this.endDate.Name = "endDate";
            this.endDate.OnFocusColor = System.Drawing.Color.MediumSeaGreen;
            this.endDate.Padding = new System.Windows.Forms.Padding(0, 2, 1, 2);
            this.endDate.Size = new System.Drawing.Size(97, 30);
            this.endDate.TabIndex = 0;
            this.endDate.Value = new System.DateTime(2022, 2, 1, 19, 5, 33, 501);
            this.endDate.ValueChanging += new System.EventHandler(this.endDate_ValueChanging);
            // 
            // startDate
            // 
            this.startDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.startDate.Caption = "Caption text";
            this.startDate.CaptionFont = new System.Drawing.Font("Verdana", 9.25F);
            this.startDate.CaptionForeColor = System.Drawing.Color.DimGray;
            this.startDate.CaptionVisible = false;
            this.startDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.startDate.Enabled = false;
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDate.Info = "";
            this.startDate.InfoFont = new System.Drawing.Font("Verdana", 8.25F);
            this.startDate.InfoForeColor = System.Drawing.Color.Gray;
            this.startDate.InfoVisible = false;
            this.startDate.LineColor = System.Drawing.Color.Silver;
            this.startDate.Location = new System.Drawing.Point(192, 1);
            this.startDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.startDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.startDate.MinimumSize = new System.Drawing.Size(12, 12);
            this.startDate.Name = "startDate";
            this.startDate.OnFocusColor = System.Drawing.Color.MediumSeaGreen;
            this.startDate.Padding = new System.Windows.Forms.Padding(0, 2, 1, 2);
            this.startDate.Size = new System.Drawing.Size(101, 30);
            this.startDate.TabIndex = 0;
            this.startDate.Value = new System.DateTime(2022, 2, 1, 19, 5, 33, 501);
            this.startDate.ValueChanging += new System.EventHandler(this.startDate_ValueChanging);
            // 
            // _bot
            // 
            this._bot.BackColor = System.Drawing.Color.White;
            this._bot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._bot.Location = new System.Drawing.Point(73, 28);
            this._bot.Name = "_bot";
            this._bot.Size = new System.Drawing.Size(79, 3);
            this._bot.TabIndex = 1;
            // 
            // divHeader
            // 
            this.divHeader.Controls.Add(this._user);
            this.divHeader.Controls.Add(this._title);
            this.divHeader.Controls.Add(this._add);
            this.divHeader.Controls.Add(this._minimize);
            this.divHeader.Controls.Add(this._exit);
            this.divHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.divHeader.Location = new System.Drawing.Point(0, 0);
            this.divHeader.Name = "divHeader";
            this.divHeader.Size = new System.Drawing.Size(392, 32);
            this.divHeader.TabIndex = 0;
            // 
            // _user
            // 
            this._user.AutoSize = true;
            this._user.BackColor = System.Drawing.Color.WhiteSmoke;
            this._user.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._user.ForeColor = System.Drawing.Color.Gray;
            this._user.Location = new System.Drawing.Point(33, 18);
            this._user.Name = "_user";
            this._user.Size = new System.Drawing.Size(23, 12);
            this._user.TabIndex = 7;
            this._user.Text = "user";
            this._user.MouseDown += new System.Windows.Forms.MouseEventHandler(this._user_MouseDown);
            // 
            // _title
            // 
            this._title.BackColor = System.Drawing.Color.WhiteSmoke;
            this._title.Dock = System.Windows.Forms.DockStyle.Fill;
            this._title.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._title.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._title.Location = new System.Drawing.Point(32, 0);
            this._title.Name = "_title";
            this._title.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this._title.Size = new System.Drawing.Size(296, 32);
            this._title.TabIndex = 0;
            this._title.Text = "JCP HOME-WORK";
            this._title.MouseDown += new System.Windows.Forms.MouseEventHandler(this._user_MouseDown);
            // 
            // _minimize
            // 
            this._minimize.BackColor = System.Drawing.Color.WhiteSmoke;
            this._minimize.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this._minimize.BorderColor = System.Drawing.Color.Gainsboro;
            this._minimize.BorderRadius = 6;
            this._minimize.BorderSize = 1;
            this._minimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this._minimize.Dock = System.Windows.Forms.DockStyle.Right;
            this._minimize.FlatAppearance.BorderSize = 0;
            this._minimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._minimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this._minimize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._minimize.Location = new System.Drawing.Point(328, 0);
            this._minimize.Name = "_minimize";
            this._minimize.Size = new System.Drawing.Size(32, 32);
            this._minimize.TabIndex = 5;
            this._minimize.Text = "─";
            this._minimize.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._minimize.UseVisualStyleBackColor = false;
            this._minimize.Click += new System.EventHandler(this._minimize_Click);
            // 
            // _exit
            // 
            this._exit.BackColor = System.Drawing.Color.WhiteSmoke;
            this._exit.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this._exit.BorderColor = System.Drawing.Color.Gainsboro;
            this._exit.BorderRadius = 6;
            this._exit.BorderSize = 1;
            this._exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this._exit.Dock = System.Windows.Forms.DockStyle.Right;
            this._exit.FlatAppearance.BorderSize = 0;
            this._exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this._exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._exit.Location = new System.Drawing.Point(360, 0);
            this._exit.Name = "_exit";
            this._exit.Size = new System.Drawing.Size(32, 32);
            this._exit.TabIndex = 4;
            this._exit.Text = "x";
            this._exit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._exit.UseVisualStyleBackColor = false;
            this._exit.Click += new System.EventHandler(this._exit_Click);
            // 
            // cbxDate
            // 
            this.cbxDate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.cbxDate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxDate.Image = ((System.Drawing.Image)(resources.GetObject("cbxDate.Image")));
            this.cbxDate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbxDate.Location = new System.Drawing.Point(152, 1);
            this.cbxDate.Name = "cbxDate";
            this.cbxDate.Size = new System.Drawing.Size(40, 30);
            this.cbxDate.TabIndex = 6;
            this.cbxDate.UseVisualStyleBackColor = true;
            this.cbxDate.CheckedChanged += new System.EventHandler(this.cbxDate_CheckedChanged);
            // 
            // cbxVer
            // 
            this.cbxVer.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxVer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxVer.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbxVer.FlatAppearance.BorderSize = 0;
            this.cbxVer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxVer.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVer.Image = global::Autonomus.Notes.Properties.Resources.selective_highlighting_20px;
            this.cbxVer.Location = new System.Drawing.Point(34, 1);
            this.cbxVer.Name = "cbxVer";
            this.cbxVer.Size = new System.Drawing.Size(39, 30);
            this.cbxVer.TabIndex = 5;
            this.cbxVer.UseVisualStyleBackColor = true;
            this.cbxVer.CheckedChanged += new System.EventHandler(this.cbxVer_CheckedChanged);
            // 
            // _add
            // 
            this._add.BackColor = System.Drawing.Color.WhiteSmoke;
            this._add.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this._add.BorderColor = System.Drawing.Color.Gainsboro;
            this._add.BorderRadius = 6;
            this._add.BorderSize = 1;
            this._add.Cursor = System.Windows.Forms.Cursors.Hand;
            this._add.Dock = System.Windows.Forms.DockStyle.Left;
            this._add.FlatAppearance.BorderSize = 0;
            this._add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this._add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._add.Image = ((System.Drawing.Image)(resources.GetObject("_add.Image")));
            this._add.Location = new System.Drawing.Point(0, 0);
            this._add.Name = "_add";
            this._add.Size = new System.Drawing.Size(32, 32);
            this._add.TabIndex = 6;
            this._add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._add.UseVisualStyleBackColor = false;
            this._add.Click += new System.EventHandler(this._add_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRefresh.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnRefresh.BorderColor = System.Drawing.Color.Gainsboro;
            this.btnRefresh.BorderRadius = 6;
            this.btnRefresh.BorderSize = 1;
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnRefresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(2, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(32, 30);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // StickyView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(408, 571);
            this.ControlBox = false;
            this.Controls.Add(this.divContent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "StickyView";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.StickyView_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StickyView_KeyPress);
            this.divContent.ResumeLayout(false);
            this.divBody.ResumeLayout(false);
            this.divFooter.ResumeLayout(false);
            this.divHeader.ResumeLayout(false);
            this.divHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel divContent;
        private System.Windows.Forms.Label _divider;
        private System.Windows.Forms.Panel divBody;
        private System.Windows.Forms.FlowLayoutPanel flowContainer;
        private System.Windows.Forms.Panel divFooter;
        private Autonomus.SDK.Components.CustomControls.AutonomusDateTimePicker endDate;
        private Autonomus.SDK.Components.CustomControls.AutonomusDateTimePicker startDate;
        private System.Windows.Forms.Label _bot;
        private System.Windows.Forms.CheckBox cbxDate;
        private System.Windows.Forms.CheckBox cbxVer;
        private System.Windows.Forms.Panel divHeader;
        private System.Windows.Forms.Label _user;
        private System.Windows.Forms.Label _title;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton _add;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton _minimize;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton _exit;
        public Autonomus.SDK.Components.CustomControls.AutonomusButton btnRefresh;
    }
}