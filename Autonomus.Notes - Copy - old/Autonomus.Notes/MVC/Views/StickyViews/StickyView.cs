﻿using Autonomus.Notes.Helpers.Items;
using Autonomus.Notes.MVC.Controllers.StickyControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomus.Notes.MVC.Views.StickyViews
{
    public partial class StickyView : Form
    {
        StickyController _controller;
        public StickyView()
        {
            InitializeComponent();
            new Autonomus.SDK.Components.CustomClass.ShadowBox().Form(this);
            startDate.Value = SDK.Getting.GetFirtDayMonth();
            endDate.Value = DateTime.Now;
        }

        void addedAction()
        {
            _title.Text = $"SDK HOME-WORK  ({flowContainer.Controls.Count} - Notes)";
            _user.Text = $"{Program.Nombres} - {Program.Descripcion}";
        }

        private async void adding()
        {
            var response = await _controller.SaveNote(new Domain.Models.StickyNote()
            {
                Id = "",
                Cliente = "",
                Content = "",
                Completed = false,
                IdUser = Program.IdUsuario,
                Priority = false,
                Deleted = false
            }, Helpers.Enums.Crud.Insert);

            if (response > 0)
                charge();
        }


        private void charge()
        {
            _controller.ChargeData(new Domain.Queries.StickyQueries.StickyQueryParams()
            {
                @Completed = cbxVer.Checked,
                @IdUser = Program.IdUsuario,
                @FilterDate = cbxDate.Checked,
                @StartDate = startDate.Value,
                @EndDate = endDate.Value
            }, flowContainer);
        }

        private void cbxVer_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxVer.Checked)
                cbxVer.Image = Properties.Resources.smartphone_approved_20px;
            else
                cbxVer.Image = Properties.Resources.selective_highlighting_20px;

            charge();
        }

        private void cbxDate_CheckedChanged(object sender, EventArgs e)
        {
            startDate.Enabled = cbxDate.Checked;
            endDate.Enabled = cbxDate.Checked;
            charge();
        }

        private void StickyView_Load(object sender, EventArgs e)
        {
            Rectangle res = Screen.PrimaryScreen.Bounds;
            this.Location = new Point(res.Width - (Size.Width + 12), res.Height - (Size.Height + 60));

            _controller = new StickyController();
            charge();
            addedAction();
        }



        private void _user_MouseDown(object sender, MouseEventArgs e)
        {
            Autonomus.SDK.Components.CustomClass.MoveControls.Form(this);
        }

        private void _exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void _minimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void StickyView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void startDate_ValueChanging(object sender, EventArgs e)
        {
            if (cbxDate.Checked)
                charge();
        }

        private void _add_Click(object sender, EventArgs e)
        {
            adding();
        }



        private void flowContainer_ControlRemoved(object sender, ControlEventArgs e)
        {
            addedAction();
        }

        private void flowContainer_ControlAdded(object sender, ControlEventArgs e)
        {
            addedAction();
        }

        private void endDate_ValueChanging(object sender, EventArgs e)
        {
            if (cbxDate.Checked)
                charge();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            charge();
        }
    }
}
