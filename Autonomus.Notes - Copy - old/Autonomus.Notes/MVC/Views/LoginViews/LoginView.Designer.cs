﻿namespace Autonomus.Notes.MVC.Views.LoginViews
{
    partial class LoginView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginView));
            this.divHeader = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMinimizar = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.btnSalir = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.divContent = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnLogin = new Autonomus.SDK.Components.CustomControls.AutonomusButton();
            this.txPassword = new Autonomus.SDK.Components.CustomControls.AutonomusTextBox();
            this.txLogin = new Autonomus.SDK.Components.CustomControls.AutonomusTextBox();
            this.divHeader.SuspendLayout();
            this.divContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // divHeader
            // 
            this.divHeader.Controls.Add(this.label1);
            this.divHeader.Controls.Add(this.btnMinimizar);
            this.divHeader.Controls.Add(this.btnSalir);
            this.divHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.divHeader.Location = new System.Drawing.Point(8, 8);
            this.divHeader.Name = "divHeader";
            this.divHeader.Size = new System.Drawing.Size(452, 26);
            this.divHeader.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(381, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sticky Notes {Autonomus SDK} - Iniciar sesión";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnMinimizar.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnMinimizar.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnMinimizar.BorderRadius = 0;
            this.btnMinimizar.BorderSize = 0;
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnMinimizar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMinimizar.Location = new System.Drawing.Point(381, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(37, 26);
            this.btnMinimizar.TabIndex = 5;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMinimizar.UseVisualStyleBackColor = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSalir.BackGroundColor = System.Drawing.Color.WhiteSmoke;
            this.btnSalir.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnSalir.BorderRadius = 0;
            this.btnSalir.BorderSize = 0;
            this.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSalir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSalir.Location = new System.Drawing.Point(418, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(34, 26);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "x";
            this.btnSalir.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // divContent
            // 
            this.divContent.Controls.Add(this.pictureBox1);
            this.divContent.Controls.Add(this.btnLogin);
            this.divContent.Controls.Add(this.txPassword);
            this.divContent.Controls.Add(this.txLogin);
            this.divContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divContent.Location = new System.Drawing.Point(8, 34);
            this.divContent.Name = "divContent";
            this.divContent.Size = new System.Drawing.Size(452, 199);
            this.divContent.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 176);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(117)))), ((int)(((byte)(183)))));
            this.btnLogin.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(117)))), ((int)(((byte)(183)))));
            this.btnLogin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(189)))), ((int)(((byte)(255)))));
            this.btnLogin.BorderRadius = 6;
            this.btnLogin.BorderSize = 1;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(204, 142);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(225, 40);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "LOGIN";
            this.btnLogin.TextColor = System.Drawing.Color.White;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txPassword
            // 
            this.txPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.txPassword.Caption = "";
            this.txPassword.CaptionFont = new System.Drawing.Font("Verdana", 9.25F);
            this.txPassword.CaptionForeColor = System.Drawing.Color.DimGray;
            this.txPassword.CaptionHeight = 6;
            this.txPassword.CaptionVisible = false;
            this.txPassword.Category = '.';
            this.txPassword.ClearVisible = false;
            this.txPassword.ExpandText = Autonomus.SDK.Components.Models.Enums.ExpandText.Fill;
            this.txPassword.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPassword.Icon = ((System.Drawing.Image)(resources.GetObject("txPassword.Icon")));
            this.txPassword.IconVisible = true;
            this.txPassword.Info = "";
            this.txPassword.InfoFont = new System.Drawing.Font("Verdana", 8.25F);
            this.txPassword.InfoForeColor = System.Drawing.Color.Gray;
            this.txPassword.InfoVisible = false;
            this.txPassword.InputValidation = Autonomus.SDK.Components.Models.Enums.InputValidation.None;
            this.txPassword.IsPlaceholder = true;
            this.txPassword.LineColor = System.Drawing.Color.Silver;
            this.txPassword.Location = new System.Drawing.Point(204, 86);
            this.txPassword.MinimumSize = new System.Drawing.Size(60, 32);
            this.txPassword.Multiline = false;
            this.txPassword.Name = "txPassword";
            this.txPassword.OnFocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(189)))), ((int)(((byte)(255)))));
            this.txPassword.Padding = new System.Windows.Forms.Padding(6, 2, 2, 2);
            this.txPassword.PasswordChar = '•';
            this.txPassword.Placeholder = "Contraseña";
            this.txPassword.PlaceholderColor = System.Drawing.Color.DimGray;
            this.txPassword.PlaceholderFont = new System.Drawing.Font("Verdana", 10F);
            this.txPassword.Size = new System.Drawing.Size(225, 32);
            this.txPassword.TabIndex = 1;
            this.txPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txPassword_KeyDown);
            // 
            // txLogin
            // 
            this.txLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.txLogin.Caption = "";
            this.txLogin.CaptionFont = new System.Drawing.Font("Verdana", 9.25F);
            this.txLogin.CaptionForeColor = System.Drawing.Color.DimGray;
            this.txLogin.CaptionHeight = 6;
            this.txLogin.CaptionVisible = false;
            this.txLogin.Category = '.';
            this.txLogin.ClearVisible = false;
            this.txLogin.ExpandText = Autonomus.SDK.Components.Models.Enums.ExpandText.Fill;
            this.txLogin.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txLogin.Icon = ((System.Drawing.Image)(resources.GetObject("txLogin.Icon")));
            this.txLogin.IconVisible = true;
            this.txLogin.Info = "";
            this.txLogin.InfoFont = new System.Drawing.Font("Verdana", 8.25F);
            this.txLogin.InfoForeColor = System.Drawing.Color.Gray;
            this.txLogin.InfoVisible = false;
            this.txLogin.InputValidation = Autonomus.SDK.Components.Models.Enums.InputValidation.None;
            this.txLogin.IsPlaceholder = true;
            this.txLogin.LineColor = System.Drawing.Color.Silver;
            this.txLogin.Location = new System.Drawing.Point(204, 39);
            this.txLogin.MinimumSize = new System.Drawing.Size(60, 32);
            this.txLogin.Multiline = false;
            this.txLogin.Name = "txLogin";
            this.txLogin.OnFocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(189)))), ((int)(((byte)(255)))));
            this.txLogin.Padding = new System.Windows.Forms.Padding(6, 2, 2, 2);
            this.txLogin.Placeholder = "Usuario";
            this.txLogin.PlaceholderColor = System.Drawing.Color.DimGray;
            this.txLogin.PlaceholderFont = new System.Drawing.Font("Verdana", 10F);
            this.txLogin.Size = new System.Drawing.Size(225, 32);
            this.txLogin.TabIndex = 0;
            this.txLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txLogin_KeyDown);
            this.txLogin.Leave += new System.EventHandler(this.txLogin_Leave);
            // 
            // LoginView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(468, 241);
            this.ControlBox = false;
            this.Controls.Add(this.divContent);
            this.Controls.Add(this.divHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "LoginView";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.LoginView_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LoginView_KeyPress);
            this.divHeader.ResumeLayout(false);
            this.divContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel divHeader;
        private System.Windows.Forms.Panel divContent;
        private System.Windows.Forms.Label label1;
        private Autonomus.SDK.Components.CustomControls.AutonomusButton btnMinimizar;
        private Autonomus.SDK.Components.CustomControls.AutonomusButton btnSalir;
        private Autonomus.SDK.Components.CustomControls.AutonomusButton btnLogin;
        private Autonomus.SDK.Components.CustomControls.AutonomusTextBox txPassword;
        private Autonomus.SDK.Components.CustomControls.AutonomusTextBox txLogin;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}