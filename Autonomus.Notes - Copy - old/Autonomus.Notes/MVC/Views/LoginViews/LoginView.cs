﻿using Autonomus.Notes.MVC.Controllers;
using Autonomus.Notes.MVC.Controllers.LoginControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.MVC.Views.LoginViews
{
    public partial class LoginView : Form
    {
        LoginController _controller;
        public Login Login;
        public LoginView()
        {
            InitializeComponent();
            Login = Login.Wrong;
        }

        void access()
        {
            var response = _controller.ValidPassword(txLogin.Text,txPassword.Text);
            if (response)
            {
                Login = Login.Success;
                this.Close();
            }
            else
            {
                Login = Login.Wrong;
                SDK.MessageBoxs.Warning("Usuario o Contraseña inválido", " El usuario o la contraseña que se ha ingresado no es correcto....");
            }
        }


        private void LoginView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            Autonomus.SDK.Components.CustomClass.MoveControls.Form(this);
        }

        private void LoginView_Load(object sender, EventArgs e)
        {
            _controller = new LoginController();

        }

        private void txLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                _controller.GetUsuarios(txLogin.Text);
        }

        private void txLogin_Leave(object sender, EventArgs e)
        {
            _controller.GetUsuarios(txLogin.Text);
        }

        private void txPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                access();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            access();
        }
    }
}
