﻿using Autonomus.Notes.DataAccess;
using Autonomus.Notes.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomus.Notes.MVC.Controllers.LoginControllers
{
    public class LoginController
    {
        List<Usuary> _user;
        public LoginController() { }

        // validar usuario
        public async void GetUsuarios(string username)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var response = await uow.Users.GetLogin(username.ToLower().Trim());
                _user = new List<Usuary>();
                _user = response.ToList();
            }
        }


        //validar  contraseña
        public bool ValidPassword(string username, string password)
        {
            if (_user != null && _user.Count > 0)
            {
                var exist = _user.FirstOrDefault(o => o.UserName.ToLower() == username.ToLower().Trim()
                && o.Password == password.Trim());

                if (exist != null)
                {
                    Program.IdUsuario = exist.Id;
                    Program.Nombres = exist.FullName;
                    Program.Descripcion = exist.Description;

                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}
