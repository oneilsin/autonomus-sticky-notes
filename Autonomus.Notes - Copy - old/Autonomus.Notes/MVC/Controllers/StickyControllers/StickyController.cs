﻿using Autonomus.Notes.DataAccess;
using Autonomus.Notes.Domain.Models;
using Autonomus.Notes.Domain.Queries.StickyQueries;
using Autonomus.Notes.Helpers.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomus.Notes.Helpers.Enums;

namespace Autonomus.Notes.MVC.Controllers.StickyControllers
{
    public class StickyController
    {
        public StickyController()
        {

        }


        public async void UpdateNote(Sticky sticky)
        {
            var response = await SaveNote(new StickyNote()
            {
                Cliente = sticky.Cliente,
                Content = sticky.Text,
                Completed = sticky.State,
                Height = sticky.Height,
                Priority = sticky.Priority,
                Id = sticky.Id
            }, Crud.Update);
        }

        public async void DeleteNote(Sticky sticky)
        {
            var question = SDK.MessageBoxs.Question("Eliminar contenido",
                "Se está eliminando el contenido. Estás seguro dde continuar con esta accicón?");

            if(question == DialogResult.Yes)
            {
                var response = await SaveNote(new StickyNote()
                {
                    Deleted = true,
                    Id = sticky.Id
                }, Crud.Delete);
            }         
        }

        public async void ChargeData(StickyQueryParams @params, FlowLayoutPanel flow)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var response = await uow.Stickys.GetListado(@params);
                if (response == null) return;

                flow.Controls.Clear();

                response.OrderByDescending(o => o.Priority).ThenBy(o => o.CreatedDate).ToList()
                    .ForEach(o =>
                    {
                        var note = new Sticky()
                        {
                            Id = o.Id,
                            Index = flow.Controls.Count,
                            Date = o.CreatedDate,
                            EditedDate = o.EditedDate,
                            Text = o.Content,
                            State = o.Completed,
                            Priority = o.Priority,
                            Cliente = o.Cliente,
                            Height = o.Height,
                            Width = (flow.Width - 20)
                        };

                        note.btnUpdate.Click += (sender, args)
                          => UpdateNote(note);

                        note.btnDelete.Click += (sender, args)
                          => DeleteNote(note);

                        flow.Controls.Add(note);
                    });
            }
        }

        public async Task<int> SaveNote(StickyNote note, Crud crud)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                return await uow.Stickys.SetData(note, crud.ToString().ToLower());
            }
        }
    }
}
